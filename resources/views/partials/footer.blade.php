
	<div class="panel-footer footer">

		<div class="row link-footer">
			<div class="container">
				<ul class="link-left-footer">
					<li>
						<a href="">Trang chủ</a>
						<a href="">Phố đổ củ</a>
						<a href="">Tìm kiếm sản phẩm</a>
					</li>
				</ul>

				<ul class="link-right-footer">
					<li>
						<a href="">Quy định</a>
						<a href="">Quy chế</a>
						<a href="">Hướng dẫn sử dụng</a>
						<a href="">Liên hệ quảng cáo & dịch vụ</a>
					</li>
				</ul>
			</div>
		</div>

		
		<div class="container">
			<div class="row">
			<div class="footerMenu">
				
				<ul class="items level-0">
					<li class="level-1 col-md-3"><a href="">Thời trang nam</a>
						<ul class="items level-2">
							<li class="level-2 node_17"><a href="">Quần áo</a></li>
							<li class="level-2 node_15"><a href="">Giầy dép</a></li>
							<li class="level-2 node_14"><a href="">Đồng hồ và Trang sức</a></li>
							<li class="level-2 node_122"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_16"><a href="">Phụ kiện thời trang</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang nữ</a>
						<ul class="items level-2">
							<li class="level-2 node_50"><a href="">Quần, Áo, Váy</a></li>
							<li class="level-2 node_52"><a href="">Giầy dép</a></li>
							<li class="level-2 node_124"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_49"><a href="">Trang sức và Phụ kiện</a></li>
							<li class="level-2 node_51"><a href="">Nước hoa, Mỹ phẩm</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang cho bé</a>
						<ul class="items level-2">
							<li class="level-2 node_126"><a href="">Cho bé sơ sinh</a></li>
							<li class="level-2 node_127"><a href="">Cho bé gái</a></li>
							<li class="level-2 node_128"><a href="">Cho bé trai</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Điện thoại - Máy tính bảng</a>
						<ul class="items level-2">
							<li class="level-2 node_56"><a href="">SmartPhone, Cao cấp</a></li>
							<li class="level-2 node_94"><a href="">Điện thoại bình dân</a></li>
							<li class="level-2 node_58"><a href="">Máy tính bảng, Máy đọc sách</a></li>
							<li class="level-2 node_57"><a href="">Linh kiện, Phụ kiện</a></li>
						</ul>
					</li>
				</ul>

			</div>
			</div>
		</div> <!-- End Container -->

		<div class="container">
			<div class="row">
			<div class="footerMenu">
				
				<ul class="items level-0">
					<li class="level-1 col-md-3"><a href="">Thời trang nam</a>
						<ul class="items level-2">
							<li class="level-2 node_17"><a href="">Quần áo</a></li>
							<li class="level-2 node_15"><a href="">Giầy dép</a></li>
							<li class="level-2 node_14"><a href="">Đồng hồ và Trang sức</a></li>
							<li class="level-2 node_122"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_16"><a href="">Phụ kiện thời trang</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang nữ</a>
						<ul class="items level-2">
							<li class="level-2 node_50"><a href="">Quần, Áo, Váy</a></li>
							<li class="level-2 node_52"><a href="">Giầy dép</a></li>
							<li class="level-2 node_124"><a href="">Đồ thể thao</a></li>
							<li class="level-2 node_49"><a href="">Trang sức và Phụ kiện</a></li>
							<li class="level-2 node_51"><a href="">Nước hoa, Mỹ phẩm</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Thời trang cho bé</a>
						<ul class="items level-2">
							<li class="level-2 node_126"><a href="">Cho bé sơ sinh</a></li>
							<li class="level-2 node_127"><a href="">Cho bé gái</a></li>
							<li class="level-2 node_128"><a href="">Cho bé trai</a></li>
						</ul>
					</li>
					<li class="level-1 col-md-3"><a href="">Điện thoại - Máy tính bảng</a>
						<ul class="items level-2">
							<li class="level-2 node_56"><a href="">SmartPhone, Cao cấp</a></li>
							<li class="level-2 node_94"><a href="">Điện thoại bình dân</a></li>
							<li class="level-2 node_58"><a href="">Máy tính bảng, Máy đọc sách</a></li>
							<li class="level-2 node_57"><a href="">Linh kiện, Phụ kiện</a></li>
						</ul>
					</li>
				</ul>

			</div>
			</div>
		</div> <!-- End Container -->
	</div><!-- End .footer -->
	<div id="backtop">
		<img src="images/back-top.png" alt="">
	</div>
</body>
</html>

