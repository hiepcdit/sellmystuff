@extends('layout.master')

@section('title')
	Home page
@stop

@section('style')
	<!-- có style page thi add vào đây -->
@stop

@section('js')
	<!-- có js thì add vào đây -->
@stop

@section('link')
	<a href="#">Trang chủ</a> >>
	<a href="#">Phố đồ cũ</a> >>
	<a href="#">Tìm kiếm</a>
@stop

@section('content')
	
	<div class="row title-content">
		<a href=""><span class="panel-primary" >Máy tính, thiết bị văn phòng</span></a>
	</div>
	<div class="thumbnail">

		<img data-src="#" alt="">
		<div class="caption post">
			<img src="images/01.jpg" />
			<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
			<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
			<div class="col-md-5 info">
				<ul>
					<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
					<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
				</ul>
			</div>

			<div class="col-md-5 info2">
				<ul>
					<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
					<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
				</ul>
			</div>
		</div>
	</div><!-- end thumbnail -->

	<div class="thumbnail">

		<img data-src="#" alt="">
		<div class="caption post">
			<img src="images/01.jpg" />
			<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
			<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
			<div class="col-md-5 info">
				<ul>
					<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
					<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
				</ul>
			</div>

			<div class="col-md-5 info2">
				<ul>
					<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
					<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
				</ul>
			</div>
		</div>
	</div><!-- end thumbnail -->

	<div class="thumbnail">

		<img data-src="#" alt="">
		<div class="caption post">
			<img src="images/01.jpg" />
			<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
			<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
			<div class="col-md-5 info">
				<ul>
					<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
					<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
				</ul>
			</div>

			<div class="col-md-5 info2">
				<ul>
					<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
					<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
				</ul>
			</div>
		</div>
	</div><!-- end thumbnail -->

	<div class="row title-content">
		<a href=""><span>Điện thoại - Máy tính</span></a>
	</div>
	<div class="thumbnail">

		<img data-src="#" alt="">
		<div class="caption post">
			<img src="images/01.jpg" />
			<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
			<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
			<div class="col-md-5 info">
				<ul>
					<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
					<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
				</ul>
			</div>

			<div class="col-md-5 info2">
				<ul>
					<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
					<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
				</ul>
			</div>
		</div>
	</div><!-- end thumbnail -->

	<div class="thumbnail">

		<img data-src="#" alt="">
		<div class="caption post">
			<img src="images/01.jpg" />
			<a href=""><h5>Bán case đồng bộ core i5 và UPS giá 200.000vnd P V gia đình</h5></a>
			<span>Địa điểm: Đà Nẵng, Hồ Chí Minh, Hà Nội.</span>
			<div class="col-md-5 info">
				<ul>
					<li class="date glyphicon glyphicon-calendar"> Ngày đăng: <i>22/12/2015</i></li>
					<li class="see glyphicon glyphicon-eye-open"> Số lượng xem: <i>200</i></li>
				</ul>
			</div>

			<div class="col-md-5 info2">
				<ul>
					<li class="user glyphicon glyphicon-user"> Đăng bởi: <i>Duchiep</i></li>
					<li class="like glyphicon glyphicon-thumbs-up"> Lượt thích: <i>250</i></li>
				</ul>
			</div>
		</div>
	</div><!-- end thumbnail -->

@stop


@section('saibar')
	
	<div class="facebook">
		<div id="fb-root"></div>
		<script>
			(function(d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.3";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
		</script>
		<div class="fb-follow" data-href="https://www.facebook.com/zuck" data-width="200px" data-colorscheme="light" data-layout="standard" data-show-faces="true">

		</div>
	</div>
	<br>
	<div class="list-group">
		<a href="#" class="list-group-item active">Tin xem nhiều nhất</a>
		<a href="#" class="list-group-item">Item 2</a>
		<a href="#" class="list-group-item">Item 3</a>
		<a href="#" class="list-group-item">Item 4</a>
		<a href="#" class="list-group-item">Item 5</a>
		<a href="#" class="list-group-item">Item 6</a>
		<a href="#" class="list-group-item">Item 7</a>
	</div>
@stop
