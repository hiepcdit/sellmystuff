<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('cities', 'CityController');

Route::resource('users', 'UserController');

Route::resource('articles', 'ArticleController');

Route::controllers([
   'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

Route::get('home', function () {
    return \Auth::user();
});


get('/', ['as'=>'home','uses'=>'PagesController@index']);

Route::get('/pages/view', ['as' => 'view',"uses" => 'PagesController@view']);

