<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:2',
            'last_name'  => 'required|min:2',
            'email'      => 'required|email|unique:users',
            'password'   => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3',
            'id_card'    => 'required|numeric|min:99999999|unique:users',
            'phone'      => 'required|numeric|min:99999999',
            'city_id'    => 'required',
            'address'    => 'required|min:10',
            'birthday'   => 'required',
            'sex'        => 'required|integer|between:0,1'
        ];
    }
}
